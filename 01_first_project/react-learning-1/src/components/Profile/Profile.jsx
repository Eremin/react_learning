import React from 'react';
import MyPosts from './MyPosts/MyPosts';
import css from './Profile.module.css';

const Profile = () => {
    return (
        <div className={css.content}>
            <img alt="Profile Background" src="https://www.atlasandboots.com/wp-content/uploads/2019/05/feat-image-1-most-beautiful-mountains-in-the-world-820x312.jpg" />
            <img alt="Profile Photo" src="https://www.w3schools.com/howto/img_avatar.png" />
            <MyPosts/>
        </div>
    );
}

export default Profile;