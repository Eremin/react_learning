import React from 'react';

import Post from "./Post/Post"
import css from './MyPosts.module.css';

const MyPosts = () => {
    return (
        <div>
        <div>
            <textarea name="" id="" cols="30" rows="10"></textarea>
            <button>Add Post</button>
        </div>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        <Post/>
        </div>
    );
}

export default MyPosts;